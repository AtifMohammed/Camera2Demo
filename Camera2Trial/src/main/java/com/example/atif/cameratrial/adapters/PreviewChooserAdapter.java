package com.example.atif.cameratrial.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.atif.cameratrial.list_items.PreviewListItem;
import com.example.atif.cameratrial.R;
import com.example.atif.cameratrial.interfaces.UpdateDisplaySize;

import java.util.List;

/**
 * Created by atif on 12/02/17.
 */

public class PreviewChooserAdapter extends RecyclerView.Adapter<PreviewChooserAdapter.PreviewChooserViewHolder> {

    private List<PreviewListItem> sizes;
    private Context mContext;
    private UpdateDisplaySize updateDisplaySize;

    public PreviewChooserAdapter(Context mContext, List<PreviewListItem> sizes, UpdateDisplaySize updateDisplaySize) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.updateDisplaySize = updateDisplaySize;
    }

    @Override
    public PreviewChooserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        return new PreviewChooserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PreviewChooserViewHolder holder, int position) {
        PreviewListItem previewListItem = sizes.get(holder.getAdapterPosition());
        final Size currentSize = previewListItem.getCurrentSize();
        float megaPixel = previewListItem.getMegaPixel();
        String text = currentSize.getWidth()+"X"+currentSize.getHeight()+" "+megaPixel+" MP";
        holder.textView.setText(text);
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDisplaySize.updateDisplaySize(currentSize);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.size();
    }

    class PreviewChooserViewHolder extends RecyclerView.ViewHolder{

        private TextView textView;

        PreviewChooserViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.size_text);
        }
    }
}

package com.example.atif.cameratrial.interfaces;

import android.util.Size;

/**
 * Created by atif on 12/02/17.
 */

public interface UpdateDisplaySize {
    /**
     * <h3>Updates the camera size from list</h3>
     * @param currentSize current size of the image to be captured
     */
    void updateDisplaySize(Size currentSize);
}
